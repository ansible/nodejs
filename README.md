# nodejs and NPM

This role is based on geerlingguy's nodejs role.
All credit goes to him, any mistakes are my own.
I have essentially just stripped it down.


## Installing npm from apt did not work

The default Ubuntu 14.04 repos provide a very outdated npm that is unable to 
install any packages.

```
taha@lahore:~
$ apt-cache madison nodejs
    nodejs | 0.10.25~dfsg2-2ubuntu1.2 | http://archive.ubuntu.com/ubuntu/ trusty-updates/universe amd64 Packages
    nodejs | 0.10.25~dfsg2-2ubuntu1.2 | http://security.ubuntu.com/ubuntu/ trusty-security/universe amd64 Packages
$ apt-cache madison npm
       npm | 1.3.10~dfsg-1 | http://archive.ubuntu.com/ubuntu/ trusty/universe amd64 Packages

```

Nodejs installs OK:
```
sudo apt install nodejs
```

but even though npm installs, it does not work to install npm packages
```
sudo apt install npm
```

```
taha@lahore:~
$ sudo npm install -g npm@6.14.12 grunt-cli
npm http GET https://registry.npmjs.org/npm/6.14.12
npm http GET https://registry.npmjs.org/grunt-cli
npm http GET https://registry.npmjs.org/grunt-cli
npm http GET https://registry.npmjs.org/npm/6.14.12
npm http GET https://registry.npmjs.org/npm/6.14.12
npm http GET https://registry.npmjs.org/grunt-cli
npm ERR! Error: CERT_UNTRUSTED
npm ERR!     at SecurePair.<anonymous> (tls.js:1370:32)
npm ERR!     at SecurePair.EventEmitter.emit (events.js:92:17)
npm ERR!     at SecurePair.maybeInitFinished (tls.js:982:10)
npm ERR!     at CleartextStream.read [as _read] (tls.js:469:13)
npm ERR!     at CleartextStream.Readable.read (_stream_readable.js:320:10)
npm ERR!     at EncryptedStream.write [as _write] (tls.js:366:25)
npm ERR!     at doWrite (_stream_writable.js:223:10)
npm ERR!     at writeOrBuffer (_stream_writable.js:213:5)
npm ERR!     at EncryptedStream.Writable.write (_stream_writable.js:180:11)
npm ERR!     at write (_stream_readable.js:583:24)
npm ERR!     at flow (_stream_readable.js:592:7)
npm ERR!     at Socket.pipeOnReadable (_stream_readable.js:624:5)
npm ERR! If you need help, you may report this log at:
npm ERR!     <http://github.com/isaacs/npm/issues>
npm ERR! or email it to:
npm ERR!     <npm-@googlegroups.com>

npm ERR! System Linux 4.15.0-147-generic
npm ERR! command "/usr/bin/nodejs" "/usr/bin/npm" "install" "-g" "npm@6.14.12" "grunt-cli"
npm ERR! cwd /home/taha
npm ERR! node -v v0.10.25
npm ERR! npm -v 1.3.10
npm ERR! 
npm ERR! Additional logging details can be found in:
npm ERR!     /home/taha/npm-debug.log
npm ERR! not ok code 0
```



## Refs

+ https://github.com/geerlingguy/ansible-role-nodejs
+ https://docs.ansible.com/ansible/latest/modules/npm_module.html
