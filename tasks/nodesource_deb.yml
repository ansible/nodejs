---

# Install from DEB files to avoid the head-aches that come from unpacking archives manually

# https://github.com/nodesource/distributions/issues/33
# https://deb.nodesource.com/node_10.x/pool/main/n/nodejs/
# https://deb.nodesource.com/node_10.x/pool/main/n/nodejs/nodejs_10.24.1-1nodesource1_amd64.deb
# I placed a copy of this DEB in files/ just in case the site goes down in the future
# Yeah, definitely better to download manually over HTTPS then install the DEB file using Ansible
# than to download over HTTP and install in Ansible in one step

- name: Copy NodeJS DEB file to target
  ansible.builtin.copy:
    src: "nodejs_{{ nodejs_version }}-1nodesource1_amd64.deb"
    dest: "{{ ansible_env.HOME }}/.cache/"
    owner: "{{ ansible_env.USER }}"
    group: "{{ ansible_env.USER }}"
  register: nodejs_deb_path

# installs node, nodejs v10.24.1 and npm v6.14.12
# turns out the target Ubuntu 14.04 is so old that we get SSL certificate errors if we attempt to use https:
# fatal: [lahore]: FAILED! => {"changed": false,
# "msg": "Failed to validate the SSL certificate for deb.nodesource.com:443.
# Make sure your managed systems have a valid CA certificate installed. If the website
# serving the url uses SNI you need python >= 2.7.9 on your managed machine  (the python
# executable used (/usr/bin/python2) is version: 2.7.6 (default, Nov 13 2018, 12:45:42) [GCC 4.8.4])
# or you can install the `urllib3`, `pyOpenSSL`, `ndg-httpsclient`, and `pyasn1` python modules to
# perform SNI verification in python >= 2.6. You can use validate_certs=False if you do not need to
# confirm the servers identity but this is unsafe and not recommended. Paths checked for this platform:
# /etc/ssl/certs, /etc/pki/ca-trust/extracted/pem, /etc/pki/tls/certs, /usr/share/ca-certificates/cacert.org,
# /etc/ansible. The exception msg was: hostname 'deb.nodesource.com' doesn't match either of 'a248.e.akamai.net',
# '*.akamaized.net', '*.akamaized-staging.net', '*.akamaihd.net', '*.akamaihd-staging.net'.",
# "status": -1, "url": "https://deb.nodesource.com/node_10.x/pool/main/n/nodejs/nodejs_10.24.1-1nodesource1_amd64.deb"}
- name: "Install NodeJS v{{ nodejs_version }} using DEB from NodeSource"
  ansible.builtin.apt:
    deb: "{{ nodejs_deb_path.dest }}"

- name: Install global NPM packages
  community.general.npm:
    name: "{{ item.name | default(item) }}"
    version: "{{ item.version | default('latest') }}"
    global: "{{ nodejs_npm_library_global | bool }}"
    state: present
  loop: "{{ nodejs_npm_packages }}"
