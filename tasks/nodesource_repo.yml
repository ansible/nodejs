---

# based on upstream's own setup_XX.x scripts (see ./files/), I expect these
# tasks to work for Nodejs >= 18.x, and maybe for 16.x.

- name: Ensure NodeJS dependencies are installed
  ansible.builtin.apt:
    name:
      - apt-transport-https
      - gnupg2
    state: present

# taha@asks2:/media/bay/taha/projects/ansible/pub/roles/nodejs/files
# $ gpg --show-keys nodesource-repo.gpg.key
# pub   rsa2048 2016-05-23 [SC]
#       6F71F525282841EEDAF851B42F59B5F99B1BE0B4
# uid                      NSolid <nsolid-gpg@nodesource.com>
# sub   rsa2048 2016-05-23 [E]
# https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_key_module.html
- name: Add Nodesource signing key
  ansible.builtin.apt_key:
    url: https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key
    id: "6F71F525282841EEDAF851B42F59B5F99B1BE0B4"
    keyring: "{{ nodejs_keyring_path }}"
    state: present

- name: Add NodeSource repositories for Node.js
  ansible.builtin.apt_repository:
    repo: "{{ item }}"
    state: present
    update_cache: yes
  loop:
    - "deb [signed-by={{ nodejs_keyring_path }}] https://deb.nodesource.com/node_{{ nodejs_version }} nodistro main"
    - "deb-src [signed-by={{ nodejs_keyring_path }}] https://deb.nodesource.com/node_{{ nodejs_version }} nodistro main"

- name: "Install Node.js v{{ nodejs_version }}"
  ansible.builtin.apt:
    name: "nodejs={{ nodejs_version | regex_replace('x', '') }}*"
    state: present

- name: Define nodejs_install_npm_user
  ansible.builtin.set_fact:
    nodejs_install_npm_user: "{{ ansible_user | default(lookup('env', 'USER')) }}"
  when: nodejs_install_npm_user is not defined

- name: Create npm global directory
  ansible.builtin.file:
    path: "{{ npm_config_prefix }}" # "{{ ansible_env.HOME }}/.local/lib/npm"
    owner: "{{ nodejs_install_npm_user }}"
    group: "{{ nodejs_install_npm_user }}"
    state: directory

- name: Add npm_config_prefix bin directory to global $PATH.
  ansible.builtin.template:
    src: npm.sh.j2
    dest: /etc/profile.d/npm.sh
    mode: 0644

# If one or more npm package install fails with "ERR! code 217 git dep preparation failed",
# fix by nuking the cache and wiping the node_modules directory
# $ npm cache clean --force
# $ rm -rf /usr/local/lib/npm/lib/node_modules
# https://stackoverflow.com/questions/66434750/npm-err-git-dep-preparation-failed-when-trying-to-install-package-json
# https://stackoverflow.com/questions/73885091/npm-error-git-dep-preparation-failed-while-installing-a-git-repo
# Actually this failure keeps happening again on every re-run, so let's add this as tasks.
# Note: this might be happening on taipei only because we are working with the rather old Nodejs 14.x
- name: Wipe NPM cache
  ansible.builtin.command: "/usr/bin/npm cache clean --force"
  when: nodejs_version == "14.x"

- name: Wipe {{ npm_config_prefix }}/lib/node_modules
  ansible.builtin.file:
    path: "{{ npm_config_prefix }}/lib/node_modules"
    state: absent
  when: nodejs_version == "14.x"

# THE FIX probably lies here: https://github.com/npm/cli/issues/624#issuecomment-1003870236
# env: PATH: is needed if installed using tarball or nodesource repo
# https://docs.npmjs.com/cli/v8/commands/npm-install
- name: Install NPM packages
  community.general.npm:
    # note, using git URL is tricky, make sure you use correct notation
    # https://github.com/npm/npm/issues/16980
    # https://github.com/npm/cli/issues/624
    name: "{{ item.name | default(item) }}"
    # note: appending @latest by default works well except when name is an URL to git repo
    # in that case, you should explicitly set version: "" to avoid error: "An unknown git error occurred"
    # I expect git repos as names to be rare occurrence, so not changing default behaviour at this time
    version: "{{ item.version | default('latest') }}"
    global: "{{ nodejs_npm_library_global | bool }}"
    # if global: false, path must be specified
    path: "{{ npm_config_prefix }}"
    unsafe_perm: "{{ npm_config_unsafe_perm | bool }}"
    state: present
  environment:
    PATH: "{{ npm_config_prefix }}/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"
    NPM_CONFIG_PREFIX: "{{ npm_config_prefix }}"
    NODE_PATH: "{{ npm_config_prefix }}/lib/node_modules"
  loop: "{{ nodejs_npm_packages }}"
